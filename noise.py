import numpy as np
import matplotlib.pyplot as plt
from PIL import Image
from itertools import product

def medianFilter(img, mask_size =3):
    img = np.asarray(img)
    width, height = img.shape
    prod_iter = product(range(width), range(height))
    central_point_idx = int(mask_size / 2)
    result_img = img.copy()
    for i,j in prod_iter:
        sliced = img[i:min(i+mask_size, width), j:min(j+mask_size, height)]
        result_img[i, j] = sliced.mean()
    return result_img