import numpy as np
import matplotlib.pyplot as plt
from PIL import Image
import hist as imp
import noise as no
import blur as bl
import cv2
import grad as gd

def showImages(imgs):
    fig = plt.figure()
    fig.set_figheight(20)
    fig.set_figwidth(20)

    for i in range(len(imgs)):
        fig.add_subplot(1, len(imgs), i + 1)
        plt.imshow(imgs[i], cmap='gray')

    plt.show(block=True)

img = Image.open('img/sample.gif').convert("L")
# img = Image.open('img/sap.png').convert("L")
linearImg = imp.histogramLinear(img, 150, 255)
imgEq = imp.histogramEq(img)
median = no.medianFilter(img, 3)
gaussian = bl.gaussian_blur(img, 9)
sobel, laplas = gd.calcImgCharacteristics(img)
showImages([img, linearImg, imgEq, median, gaussian, sobel, laplas, cv2.Canny(np.asarray(img), 100, 200)])

