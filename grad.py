import numpy as np
import cv2
import random as r

def calcImgCharacteristics(img):
    img = np.asarray(img)
    # return cv2.Canny(np.asarray(img), 100, 200)
    laplacian = cv2.Laplacian(img, cv2.CV_64F)
    sobelx = cv2.Sobel(img, cv2.CV_64F, 1, 0, ksize=5)
    sobely = cv2.Sobel(img, cv2.CV_64F, 0, 1, ksize=5)
    _printRandomGradValue(sobelx)
    return [sobelx, laplacian]
    # return np.hypot(sobelx, sobely)

def _printRandomGradValue(imgGradient):
    width, height = imgGradient.shape
    x = r.randrange(0, width)
    y = r.randrange(0, height)
    print('Grad value of X:' + str(x) + 'Y:' + str(y) + 'is' + str(imgGradient[x][y]))
