import numpy as np
import matplotlib.pyplot as plt
from PIL import Image

def histogramLinear(image, f_from = 0, f_max = 255):
    img_array = np.asarray(image)
    img_array_flat = img_array.flatten().astype('float')
    f_min = img_array_flat.min()
    f_max = img_array_flat.max()
    a = 255 * (f_min / (f_min - f_max))
    b = 255 / (f_max - f_min)

    result = []
    for pixel_value in img_array_flat:
        if pixel_value > f_from and pixel_value < f_max:
            result.append(int(a + (b * pixel_value)))
        else:
            result.append(pixel_value)

    return np.reshape(np.array(result).astype('uint8'), img_array.shape)

def histogramEq(image):
    img_array = np.asarray(image)
    img_array_flat = img_array.flatten()
    hist = get_histogram(img_array_flat, 256)
    cs = cumsum(hist)
    nj = (cs - cs.min()) * 255
    N = cs.max() - cs.min()
    cs = nj / N
    cs = cs.astype('uint8')
    img_new = cs[img_array_flat]
    return np.reshape(img_new, img_array.shape)


def cumsum(a):
    a = iter(a)
    b = [next(a)]
    for i in a:
        b.append(b[-1] + i)
    return np.array(b)


def get_histogram(image, bins):
    histogram = np.zeros(bins)
    for pixel in image:
        histogram[pixel] += 1
    return histogram